package ru.t1.aksenova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.model.IUserOwnedService;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {


    public AbstractUserOwnedService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public M add(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void update(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
