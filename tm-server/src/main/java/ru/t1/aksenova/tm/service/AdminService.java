package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.IAdminService;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.exception.user.AccessDeniedException;

public final class AdminService implements IAdminService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IConnectionService connectionService;

    public AdminService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService
    ) {
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getDropScheme(@Nullable final String token) {
        @NotNull final String initToken = propertyService.getDatabaseInitToken();
        if (!initToken.equals(token) || token == null || initToken == null) throw new AccessDeniedException();
        @NotNull final String liquibaseProperty = propertyService.getLiquibasePropertiesFilename();
        @NotNull final Liquibase liquibase = connectionService.liquibase(liquibaseProperty);
        try {
            liquibase.dropAll();
            return "SUCCESS";
        } catch (@NotNull final Exception e) {
            return "ERROR";
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getInitScheme(@Nullable final String token) {
        @NotNull final String initToken = propertyService.getDatabaseInitToken();
        if (!initToken.equals(token) || token == null || initToken == null) throw new AccessDeniedException();
        @NotNull final String liquibaseProperty = propertyService.getLiquibasePropertiesFilename();
        @NotNull final Liquibase liquibase = connectionService.liquibase(liquibaseProperty);
        try {
            liquibase.update("scheme");
            return "SUCCESS";
        } catch (@NotNull final Exception e) {
            return "ERROR";
        }
    }

}
