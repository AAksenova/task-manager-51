package ru.t1.aksenova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.aksenova.tm.comparator.CreatedComparator;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;
import ru.t1.aksenova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public String getSortType(@NotNull final Comparator comparator) {
        if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == CreatedComparator.INSTANCE) return "created";
        else return "created";
    }

    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return null;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUser(entityManager.find(User.class, userId));
        entityManager.remove(model);
    }

}
