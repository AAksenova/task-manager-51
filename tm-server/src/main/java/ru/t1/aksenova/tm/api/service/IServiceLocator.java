package ru.t1.aksenova.tm.api.service;


import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    ISessionDTOService getSessionService();

    @NotNull
    IAdminService getAdminService();

}
