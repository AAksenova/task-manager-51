package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IDatabaseProperty;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;

public class ConnectionService implements IConnectionService {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();
    @NotNull
    private static Database DATABASE;
    @NotNull
    private final IDatabaseProperty databaseProperty;
    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseConnection());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseHibernateDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHibernateHbm2ddl());
        settings.put(SHOW_SQL, databaseProperty.getDatabaseHibernateShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDatabaseHibernateFormatSql());

        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseCacheUseSecondLevelCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getDatabaseCacheUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getDatabaseCacheUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getDatabaseCacheRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getDatabaseCacheRegionFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseCacheProviderConfigFileResourcePath());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public void close() {
        entityManagerFactory.close();
    }

    @Override
    @SneakyThrows
    public void closeConnectionLiquibase() {
        DATABASE.close();
    }

    @Override
    @SneakyThrows
    public void getConnectionLiquibase() {
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @Override
    @SneakyThrows
    public Liquibase liquibase(final @NotNull String filename) {
        getConnectionLiquibase();
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

    @NotNull
    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

}
