package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @NotNull
    IUserOwnedRepository<M> getRepository(@NotNull EntityManager entityManager);

    M add(@NotNull String userId, M model);

    void update(@NotNull String userId, M model);

    void remove(@NotNull String userId, M model);

}
