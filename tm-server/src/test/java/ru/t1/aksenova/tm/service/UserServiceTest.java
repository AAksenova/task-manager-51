package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ISessionDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.IUserDTOService;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.migration.AbstractSchemeTest;
import ru.t1.aksenova.tm.service.dto.ProjectDTOService;
import ru.t1.aksenova.tm.service.dto.SessionDTOService;
import ru.t1.aksenova.tm.service.dto.TaskDTOService;
import ru.t1.aksenova.tm.service.dto.UserDTOService;
import ru.t1.aksenova.tm.util.HashUtil;

import static ru.t1.aksenova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService, sessionService);

    public static void initConnectionService() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @BeforeClass
    public static void initData() throws Exception {
        initConnectionService();
    }

    @AfterClass
    public static void destroy() {
        connectionService.close();
    }

    @Before
    public void beforeTest() {
        userService.add(ADMIN_TEST);
    }

    @After
    public void afterTest() {
        @Nullable UserDTO user;
        if (userService.isLoginExist(USER_TEST_LOGIN)) {
            user = userService.findByLogin(USER_TEST_LOGIN);
            if (user != null) userService.remove(user);
        }
        if (userService.isLoginExist(USER_ADMIN_LOGIN)) {
            user = userService.findByLogin(USER_ADMIN_LOGIN);
            if (user != null) userService.remove(user);
        }
    }

    @Test
    public void create() {
        Assert.assertThrows(AbstractException.class, () -> userService.create(null, USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> userService.create("", USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> userService.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, null));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, ""));
        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_TEST.getPasswordHash()), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(AbstractException.class, () -> userService.create(null, USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> userService.create("", USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, null, USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, "", USER_TEST_EMAIL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, ADMIN_TEST.getEmail()));
        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, USER_TEST_EMAIL);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_TEST.getPasswordHash()), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(AbstractException.class, () -> userService.create(null, USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> userService.create("", USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(ADMIN_TEST.getLogin(), USER_TEST_PASSWORD, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, null, Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, "", Role.USUAL));
        Assert.assertThrows(AbstractException.class, () -> userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, (Role) null));
        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD, Role.USUAL);
        Assert.assertEquals(USER_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_TEST.getPasswordHash()), user.getPasswordHash());
        Assert.assertEquals(USER_TEST.getRole(), user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(AbstractException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> userService.findByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> userService.findByLogin(NON_EXISTING_LOGIN));
        @Nullable final UserDTO user = userService.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(AbstractException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(AbstractException.class, () -> userService.findByEmail(""));
        Assert.assertThrows(AbstractException.class, () -> userService.findByEmail(NON_EXISTING_EMAIL));
        @Nullable final UserDTO user = userService.findByEmail(ADMIN_TEST.getEmail());
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(NON_EXISTING_LOGIN));
        assert USER_TEST.getLogin() != null;
        Assert.assertTrue(userService.isLoginExist(ADMIN_TEST.getLogin()));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(userService.isEmailExist(""));
        Assert.assertFalse(userService.isEmailExist(NON_EXISTING_EMAIL));
        assert ADMIN_TEST.getEmail() != null;
        Assert.assertTrue(userService.isEmailExist(ADMIN_TEST.getEmail()));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(AbstractException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> userService.lockUserByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> userService.lockUserByLogin(NON_EXISTING_EMAIL));
        @Nullable UserDTO user = userService.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user);
        userService.lockUserByLogin(user.getLogin());
        user = userService.findByLogin(user.getLogin());
        Assert.assertTrue(user.isLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(AbstractException.class, () -> userService.unlockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> userService.unlockUserByLogin(""));
        Assert.assertThrows(AbstractException.class, () -> userService.unlockUserByLogin(NON_EXISTING_EMAIL));
        @Nullable UserDTO user = userService.findByLogin(ADMIN_TEST.getLogin());
        Assert.assertNotNull(user);
        userService.unlockUserByLogin(user.getLogin());
        user = userService.findByLogin(user.getLogin());
        Assert.assertFalse(user.isLocked());
    }

    @Test
    public void removeOne() {
        @Nullable final UserDTO user = userService.add(USER_TEST);
        Assert.assertNotNull(userService.findOneById(USER_TEST.getId()));
        userService.remove(user);
        Assert.assertThrows(AbstractException.class, () -> userService.findOneById(USER_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> userService.removeOneById(null));
        Assert.assertThrows(AbstractException.class, () -> userService.removeOneById(""));
        @Nullable final UserDTO user = userService.add(USER_TEST);
        Assert.assertNotNull(userService.findOneById(USER_TEST.getId()));
        userService.removeOneById(user.getId());
        Assert.assertThrows(AbstractException.class, () -> userService.findOneById(USER_TEST.getId()));
    }

    @Test
    public void removeOneByEmail() {
        Assert.assertThrows(AbstractException.class, () -> userService.removeOneByEmail(null));
        Assert.assertThrows(AbstractException.class, () -> userService.removeOneByEmail(""));
        Assert.assertThrows(AbstractException.class, () -> userService.removeOneByEmail(NON_EXISTING_EMAIL));
        @Nullable final UserDTO user = userService.add(USER_TEST);
        Assert.assertNotNull(userService.findOneById(USER_TEST.getId()));
        userService.removeOneByEmail(user.getEmail());
        Assert.assertThrows(AbstractException.class, () -> userService.findOneById(USER_TEST.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> userService.findOneById(""));
        Assert.assertThrows(AbstractException.class, () -> userService.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = userService.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void setPassword() {
        Assert.assertThrows(AbstractException.class, () -> userService.setPassword(null, NON_EXISTING_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> userService.setPassword("", NON_EXISTING_PASSWORD));
        Assert.assertThrows(AbstractException.class, () -> userService.setPassword(ADMIN_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> userService.setPassword(ADMIN_TEST.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> userService.setPassword(NON_EXISTING_USER_ID, NON_EXISTING_PASSWORD));
        @Nullable final UserDTO user = userService.findOneById(ADMIN_TEST.getId());
        userService.setPassword(ADMIN_TEST.getId(), "admin2");
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        Assert.assertThrows(AbstractException.class, () -> userService.updateUser(null, FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        Assert.assertThrows(AbstractException.class, () -> userService.updateUser("", FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        Assert.assertThrows(AbstractException.class, () -> userService.updateUser(NON_EXISTING_USER_ID, FIRST_NAME, LAST_NAME, MIDDLE_NAME));
        userService.updateUser(ADMIN_TEST.getId(), FIRST_NAME, LAST_NAME, MIDDLE_NAME);
        @Nullable final UserDTO user = userService.findOneById(ADMIN_TEST.getId());
        Assert.assertEquals(FIRST_NAME, user.getFirstName());
        Assert.assertEquals(LAST_NAME, user.getLastName());
        Assert.assertEquals(MIDDLE_NAME, user.getMiddleName());
    }

}
