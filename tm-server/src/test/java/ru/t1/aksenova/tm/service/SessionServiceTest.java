package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.dto.IProjectDTOService;
import ru.t1.aksenova.tm.api.service.dto.ISessionDTOService;
import ru.t1.aksenova.tm.api.service.dto.ITaskDTOService;
import ru.t1.aksenova.tm.api.service.dto.IUserDTOService;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.migration.AbstractSchemeTest;
import ru.t1.aksenova.tm.service.dto.ProjectDTOService;
import ru.t1.aksenova.tm.service.dto.SessionDTOService;
import ru.t1.aksenova.tm.service.dto.TaskDTOService;
import ru.t1.aksenova.tm.service.dto.UserDTOService;

import java.util.Collections;
import java.util.List;

import static ru.t1.aksenova.tm.constant.SessionTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
public final class SessionServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService, sessionService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";


    public static void initConnectionService() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    public static void clearData() {
        @Nullable UserDTO user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
        user = userService.findOneById(adminId);
        if (user != null) userService.remove(user);
    }

    @BeforeClass
    public static void initData() throws Exception {
        initConnectionService();
        @NotNull final UserDTO user = userService.add(USER_TEST);
        userId = user.getId();
        @NotNull final UserDTO admin = userService.add(ADMIN_TEST);
        adminId = admin.getId();

        USER_SESSION_TEST.setUserId(userId);
        USER2_SESSION_TEST.setUserId(userId);
        ADMIN_SESSION_TEST.setUserId(adminId);
    }

    @AfterClass
    public static void destroy() {
        clearData();
        connectionService.close();
    }

    @Before
    public void beforeTest() {
        sessionService.add(USER_SESSION_TEST);
    }

    @After
    public void afterTest() {
        sessionService.removeAll(userId);
        sessionService.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(Exception.class, () -> sessionService.add(NULL_SESSION));
        Assert.assertNotNull(sessionService.add(ADMIN_SESSION_TEST));
        @Nullable final SessionDTO session = sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(ADMIN_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(Exception.class, () -> sessionService.add(NULL_SESSION));
        Assert.assertNotNull(sessionService.add(ADMIN_SESSION_TEST));
        @Nullable final SessionDTO session = sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(ADMIN_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(ADMIN_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void findAll() {
        sessionService.removeAll(userId);
        final int countRecord = sessionService.findAll(userId).size();
        sessionService.add(USER_SESSION_TEST);
        sessionService.add(USER2_SESSION_TEST);
        final List<SessionDTO> sessions = sessionService.findAll(userId);
        Assert.assertEquals(SESSION_LIST.size() + countRecord, sessions.size());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.findAll(""));
        Assert.assertEquals(Collections.emptyList(), sessionService.findAll(NON_EXISTING_SESSION_ID));
        @NotNull final List<SessionDTO> sessions = sessionService.findAll(USER_TEST.getId());
        for (SessionDTO session : sessions) {
            Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(userId, ""));
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = sessionService.findOneById(userId, USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById("", USER_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(USER_TEST.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = sessionService.findOneById(USER_TEST.getId(), USER_SESSION_TEST.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION_TEST.getUserId(), session.getUserId());
        Assert.assertEquals(USER_SESSION_TEST.getId(), session.getId());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.existsById(userId, null));
        Assert.assertFalse(sessionService.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionService.existsById(userId, USER_SESSION_TEST.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.existsById(USER_TEST.getId(), null));
        Assert.assertFalse(sessionService.existsById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        Assert.assertTrue(sessionService.existsById(USER_TEST.getId(), USER_SESSION_TEST.getId()));
    }

    @Test
    public void removeAll() {
        sessionService.removeAll(userId);
        Assert.assertEquals(0, sessionService.getSize(userId));
        sessionService.add(USER_SESSION_TEST);
        sessionService.add(USER2_SESSION_TEST);
        Assert.assertNotEquals(0, sessionService.getSize(userId));
    }


    @Test
    public void removeOne() {
        @Nullable final SessionDTO session = sessionService.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
        sessionService.remove(session);
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.removeOneById(adminId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = sessionService.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
        sessionService.removeOneById(adminId, session.getId());
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(AbstractException.class, () -> sessionService.removeOneById(null, ADMIN_SESSION_TEST.getId()));
        Assert.assertThrows(AbstractException.class, () -> sessionService.removeOneById(adminId, null));
        Assert.assertThrows(AbstractException.class, () -> sessionService.removeOneById(USER_TEST.getId(), NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = sessionService.add(ADMIN_SESSION_TEST);
        Assert.assertNotNull(sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
        sessionService.removeOneById(adminId, session.getId());
        Assert.assertThrows(AbstractException.class, () -> sessionService.findOneById(adminId, ADMIN_SESSION_TEST.getId()));
    }

}
