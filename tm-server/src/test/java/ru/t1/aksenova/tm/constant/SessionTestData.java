package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.SessionDTO;
import ru.t1.aksenova.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_TEST;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@UtilityClass
public class SessionTestData {

    @NotNull
    public final static SessionDTO USER_SESSION_TEST = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN_SESSION_TEST = new SessionDTO();

    @NotNull
    public final static SessionDTO USER2_SESSION_TEST = new SessionDTO();

    @Nullable
    public final SessionDTO NULL_SESSION = null;

    @Nullable
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = new ArrayList<>();

    @NotNull
    public final static List<SessionDTO> USER_SESSIONS_LIST = new ArrayList<>();

    static {
        USER_SESSION_TEST.setDate(new Date());
        USER_SESSION_TEST.setRole(Role.USUAL);
        USER_SESSION_TEST.setUserId(USER_TEST.getId());

        ADMIN_SESSION_TEST.setDate(new Date());
        ADMIN_SESSION_TEST.setRole(Role.ADMIN);
        ADMIN_SESSION_TEST.setUserId(ADMIN_TEST.getId());

        USER2_SESSION_TEST.setDate(new Date());
        USER2_SESSION_TEST.setRole(Role.USUAL);
        USER2_SESSION_TEST.setUserId(USER_TEST.getId());

        SESSION_LIST.add(USER2_SESSION_TEST);
        SESSION_LIST.add(ADMIN_SESSION_TEST);
        USER_SESSIONS_LIST.add(USER_SESSION_TEST);
    }

}
