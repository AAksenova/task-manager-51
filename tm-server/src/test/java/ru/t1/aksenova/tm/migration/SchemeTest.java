package ru.t1.aksenova.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import ru.t1.aksenova.tm.service.ConnectionService;
import ru.t1.aksenova.tm.service.PropertyService;

public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        final PropertyService propertyService = new PropertyService();
        final ConnectionService connectionService = new ConnectionService(propertyService);
    }

}
