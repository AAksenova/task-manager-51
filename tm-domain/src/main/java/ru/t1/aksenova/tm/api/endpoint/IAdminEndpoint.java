package ru.t1.aksenova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.request.DropSchemeRequest;
import ru.t1.aksenova.tm.dto.request.InitSchemeRequest;
import ru.t1.aksenova.tm.dto.response.DropSchemeResponse;
import ru.t1.aksenova.tm.dto.response.InitSchemeResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAdminEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AdminEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint.class);
    }

    @NotNull
    @WebMethod
    DropSchemeResponse dropDBScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DropSchemeRequest request
    );

    @NotNull
    @WebMethod
    InitSchemeResponse initDBScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull InitSchemeRequest request
    );

}
