package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.request.ApplicationAboutRequest;
import ru.t1.aksenova.tm.dto.response.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show about program.";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT CLIENT]");
        System.out.println("AUTHOR: " + service.getAuthorName());
        System.out.println("E-MAIL: " + service.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("BRANCH: " + service.getGitBranch());
        System.out.println("COMMIT ID: " + service.getGitCommitId());
        System.out.println("TIME: " + service.getGitCommitTime());
        System.out.println("MESSAGE: " + service.getGitCommitMessage());
        System.out.println("COMMITTER: " + service.getGitCommitterName());
        System.out.println("E-MAIL: " + service.getGitCommitterEmail());
        System.out.println();

        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpointClient().getAbout(request);
        System.out.println("[ABOUT SERVER]");
        System.out.println("AUTHOR: " + response.getName());
        System.out.println("E-MAIL: " + response.getEmail());
    }

}
