package ru.t1.aksenova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.request.ProjectListRequest;
import ru.t1.aksenova.tm.dto.response.ProjectListResponse;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show list projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull String sortType = TerminalUtil.nextLine();

        if (sortType.isEmpty()) {
            sortType = "BY_CREATED";
            System.out.println("DEFAULT SORT: BY_CREATED");
        }

        @Nullable final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sortType);
        @Nullable final ProjectListResponse response = getProjectEndpointClient().listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @Nullable final List<ProjectDTO> projects = response.getProjects();

        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + ": " + project.getStatus() + " ID: " + project.getId());
            index++;
        }
    }

}
