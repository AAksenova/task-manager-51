package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Link task to project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @Nullable final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpointClient().bindTaskToProject(request);
    }

}
