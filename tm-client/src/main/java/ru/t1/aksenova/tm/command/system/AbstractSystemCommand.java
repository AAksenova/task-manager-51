package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.aksenova.tm.api.service.ICommandService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ISystemEndpoint getSystemEndpointClient() {
        return getServiceLocator().getSystemEndpointClient();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpointClient() {
        return getServiceLocator().getAuthEndpointClient();
    }

    @NotNull
    protected IAdminEndpoint getAdminEndpointClient() {
        return getServiceLocator().getAdminEndpointClient();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
