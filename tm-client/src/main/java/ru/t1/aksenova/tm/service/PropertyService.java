package ru.t1.aksenova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    public static final String GIT_COMMITTER_NAME1 = "gitCommitterName";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existConfig = isExistsExternalConfig();
        if (existConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return "1.39.0";
        // return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return "Anastasiya Aksenova";
        //return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return "aaksenova@t1-consulting.ru";
        // return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_NAME1);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    public Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    public String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    public String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKay = getEnvKey(key);
        if (System.getenv().containsKey(envKay)) return System.getenv(envKay);
        return properties.getProperty(key, defaultValue);
    }

    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

}
