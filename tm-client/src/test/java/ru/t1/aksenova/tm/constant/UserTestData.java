package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static String USER_LOGIN = "usertest";

    @NotNull
    public final static String USER_PASSWORD = "usertest";

    @NotNull
    public final static String USER_EMAIL = "usertest@test.ru";

    @NotNull
    public final static String USER_LAST_NAME = "Пупкин";

    @NotNull
    public final static String USER_FIRST_NAME = "Василий";

    @NotNull
    public final static String USER_MIDDLE_NAME = "Васильевич";

    @NotNull
    public final static String ADMIN_LOGIN = "admin";

    @NotNull
    public final static String ADMIN_PASSWORD = "admin";

    @NotNull
    public final static String TEST_LOGIN = "test";

    @NotNull
    public final static String TEST_PASSWORD = "test";

    @NotNull
    public final static String TEST_EMAIL = "test@test.ru";

    @NotNull
    public final static String UPDATE_PASSWORD = "newpass";

}
