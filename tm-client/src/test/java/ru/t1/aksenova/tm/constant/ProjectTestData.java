package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class ProjectTestData {

    @NotNull
    public final static String ONE_PROJECT_NAME = "One project";

    @NotNull
    public final static String ONE_PROJECT_DESCRIPTION = "One project description";

    @NotNull
    public final static String TWO_PROJECT_NAME = "Two project";

    @NotNull
    public final static String TWO_PROJECT_DESCRIPTION = "Two project description";

    @NotNull
    public final static String THREE_PROJECT_NAME = "Three project";

    @NotNull
    public final static String THREE_PROJECT_DESCRIPTION = "Three project description";

    @NotNull
    public final static String STATUS_COMPLETE = "COMPLETED";

    @NotNull
    public final static String STATUS_IN_PROGRESS = "IN_PROGRESS";

    @NotNull
    public final static String STATUS_NOT_STARTED = "NOT_STARTED";

}
